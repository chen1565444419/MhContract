﻿namespace Sbk_Manage
{
    partial class Frm_Scan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Scan));
            this.selectSource = new System.Windows.Forms.Button();
            this.scan = new System.Windows.Forms.Button();
            this.useAdfCheckBox = new System.Windows.Forms.CheckBox();
            this.useUICheckBox = new System.Windows.Forms.CheckBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.blackAndWhiteCheckBox = new System.Windows.Forms.CheckBox();
            this.widthLabel = new System.Windows.Forms.Label();
            this.heightLabel = new System.Windows.Forms.Label();
            this.diagnosticsButton = new System.Windows.Forms.Button();
            this.checkBoxArea = new System.Windows.Forms.CheckBox();
            this.showProgressIndicatorUICheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.useDuplexCheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.autoRotateCheckBox = new System.Windows.Forms.CheckBox();
            this.autoDetectBorderCheckBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // selectSource
            // 
            this.selectSource.BackColor = System.Drawing.SystemColors.Control;
            this.selectSource.Location = new System.Drawing.Point(20, 5);
            this.selectSource.Margin = new System.Windows.Forms.Padding(5);
            this.selectSource.Name = "selectSource";
            this.selectSource.Size = new System.Drawing.Size(195, 73);
            this.selectSource.TabIndex = 0;
            this.selectSource.Text = "选择扫描仪";
            this.selectSource.UseVisualStyleBackColor = false;
            this.selectSource.Click += new System.EventHandler(this.selectSource_Click);
            // 
            // scan
            // 
            this.scan.BackColor = System.Drawing.SystemColors.Control;
            this.scan.Location = new System.Drawing.Point(20, 87);
            this.scan.Margin = new System.Windows.Forms.Padding(5);
            this.scan.Name = "scan";
            this.scan.Size = new System.Drawing.Size(195, 73);
            this.scan.TabIndex = 1;
            this.scan.Text = "开始扫描";
            this.scan.UseVisualStyleBackColor = false;
            this.scan.Click += new System.EventHandler(this.scan_Click);
            // 
            // useAdfCheckBox
            // 
            this.useAdfCheckBox.AutoSize = true;
            this.useAdfCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.useAdfCheckBox.Location = new System.Drawing.Point(20, 197);
            this.useAdfCheckBox.Margin = new System.Windows.Forms.Padding(5);
            this.useAdfCheckBox.Name = "useAdfCheckBox";
            this.useAdfCheckBox.Size = new System.Drawing.Size(107, 20);
            this.useAdfCheckBox.TabIndex = 3;
            this.useAdfCheckBox.Text = "启用送纸器";
            this.useAdfCheckBox.UseVisualStyleBackColor = false;
            // 
            // useUICheckBox
            // 
            this.useUICheckBox.AutoSize = true;
            this.useUICheckBox.BackColor = System.Drawing.Color.Transparent;
            this.useUICheckBox.Location = new System.Drawing.Point(20, 269);
            this.useUICheckBox.Margin = new System.Windows.Forms.Padding(5);
            this.useUICheckBox.Name = "useUICheckBox";
            this.useUICheckBox.Size = new System.Drawing.Size(91, 20);
            this.useUICheckBox.TabIndex = 4;
            this.useUICheckBox.Text = "显示界面";
            this.useUICheckBox.UseVisualStyleBackColor = false;
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.SystemColors.Control;
            this.saveButton.Location = new System.Drawing.Point(20, 563);
            this.saveButton.Margin = new System.Windows.Forms.Padding(5);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(195, 47);
            this.saveButton.TabIndex = 2;
            this.saveButton.Text = "保存为图片";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // blackAndWhiteCheckBox
            // 
            this.blackAndWhiteCheckBox.AutoSize = true;
            this.blackAndWhiteCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.blackAndWhiteCheckBox.Location = new System.Drawing.Point(20, 343);
            this.blackAndWhiteCheckBox.Margin = new System.Windows.Forms.Padding(5);
            this.blackAndWhiteCheckBox.Name = "blackAndWhiteCheckBox";
            this.blackAndWhiteCheckBox.Size = new System.Drawing.Size(107, 20);
            this.blackAndWhiteCheckBox.TabIndex = 6;
            this.blackAndWhiteCheckBox.Text = "扫描分辨率";
            this.blackAndWhiteCheckBox.UseVisualStyleBackColor = false;
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.BackColor = System.Drawing.Color.Transparent;
            this.widthLabel.Location = new System.Drawing.Point(20, 415);
            this.widthLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(40, 16);
            this.widthLabel.TabIndex = 7;
            this.widthLabel.Text = "宽度";
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.BackColor = System.Drawing.Color.Transparent;
            this.heightLabel.Location = new System.Drawing.Point(20, 437);
            this.heightLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(40, 16);
            this.heightLabel.TabIndex = 8;
            this.heightLabel.Text = "高度";
            // 
            // diagnosticsButton
            // 
            this.diagnosticsButton.BackColor = System.Drawing.SystemColors.Control;
            this.diagnosticsButton.Location = new System.Drawing.Point(20, 618);
            this.diagnosticsButton.Margin = new System.Windows.Forms.Padding(5);
            this.diagnosticsButton.Name = "diagnosticsButton";
            this.diagnosticsButton.Size = new System.Drawing.Size(195, 47);
            this.diagnosticsButton.TabIndex = 3;
            this.diagnosticsButton.Text = "检测扫描仪";
            this.diagnosticsButton.UseVisualStyleBackColor = false;
            this.diagnosticsButton.Click += new System.EventHandler(this.diagnostics_Click);
            // 
            // checkBoxArea
            // 
            this.checkBoxArea.AutoSize = true;
            this.checkBoxArea.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxArea.Location = new System.Drawing.Point(20, 378);
            this.checkBoxArea.Margin = new System.Windows.Forms.Padding(5);
            this.checkBoxArea.Name = "checkBoxArea";
            this.checkBoxArea.Size = new System.Drawing.Size(91, 20);
            this.checkBoxArea.TabIndex = 10;
            this.checkBoxArea.Text = "扫描区域";
            this.checkBoxArea.UseVisualStyleBackColor = false;
            // 
            // showProgressIndicatorUICheckBox
            // 
            this.showProgressIndicatorUICheckBox.AutoSize = true;
            this.showProgressIndicatorUICheckBox.BackColor = System.Drawing.Color.Transparent;
            this.showProgressIndicatorUICheckBox.Location = new System.Drawing.Point(20, 305);
            this.showProgressIndicatorUICheckBox.Margin = new System.Windows.Forms.Padding(5);
            this.showProgressIndicatorUICheckBox.Name = "showProgressIndicatorUICheckBox";
            this.showProgressIndicatorUICheckBox.Size = new System.Drawing.Size(91, 20);
            this.showProgressIndicatorUICheckBox.TabIndex = 11;
            this.showProgressIndicatorUICheckBox.Text = "显示进度";
            this.showProgressIndicatorUICheckBox.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(20, 187);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 3);
            this.label1.TabIndex = 12;
            // 
            // useDuplexCheckBox
            // 
            this.useDuplexCheckBox.AutoSize = true;
            this.useDuplexCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.useDuplexCheckBox.Location = new System.Drawing.Point(20, 231);
            this.useDuplexCheckBox.Margin = new System.Windows.Forms.Padding(5);
            this.useDuplexCheckBox.Name = "useDuplexCheckBox";
            this.useDuplexCheckBox.Size = new System.Drawing.Size(123, 20);
            this.useDuplexCheckBox.TabIndex = 13;
            this.useDuplexCheckBox.Text = "启用交互模式";
            this.useDuplexCheckBox.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(20, 261);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 3);
            this.label2.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(20, 335);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 3);
            this.label3.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(20, 473);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(200, 3);
            this.label4.TabIndex = 16;
            // 
            // autoRotateCheckBox
            // 
            this.autoRotateCheckBox.AutoSize = true;
            this.autoRotateCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.autoRotateCheckBox.Location = new System.Drawing.Point(20, 517);
            this.autoRotateCheckBox.Margin = new System.Windows.Forms.Padding(5);
            this.autoRotateCheckBox.Name = "autoRotateCheckBox";
            this.autoRotateCheckBox.Size = new System.Drawing.Size(91, 20);
            this.autoRotateCheckBox.TabIndex = 18;
            this.autoRotateCheckBox.Text = "自动旋转";
            this.autoRotateCheckBox.UseVisualStyleBackColor = false;
            // 
            // autoDetectBorderCheckBox
            // 
            this.autoDetectBorderCheckBox.AutoSize = true;
            this.autoDetectBorderCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.autoDetectBorderCheckBox.Location = new System.Drawing.Point(20, 479);
            this.autoDetectBorderCheckBox.Margin = new System.Windows.Forms.Padding(5);
            this.autoDetectBorderCheckBox.Name = "autoDetectBorderCheckBox";
            this.autoDetectBorderCheckBox.Size = new System.Drawing.Size(123, 20);
            this.autoDetectBorderCheckBox.TabIndex = 17;
            this.autoDetectBorderCheckBox.Text = "自动边缘检测";
            this.autoDetectBorderCheckBox.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(20, 546);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 3);
            this.label5.TabIndex = 19;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(245, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1024, 804);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Image = global::Sbk_Manage.Properties.Resources.about;
            this.pictureBox2.Location = new System.Drawing.Point(0, 737);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(245, 67);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            // 
            // Frm_Scan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackgroundImage = global::Sbk_Manage.Properties.Resources.timg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1269, 804);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.autoRotateCheckBox);
            this.Controls.Add(this.autoDetectBorderCheckBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.useDuplexCheckBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.showProgressIndicatorUICheckBox);
            this.Controls.Add(this.checkBoxArea);
            this.Controls.Add(this.diagnosticsButton);
            this.Controls.Add(this.heightLabel);
            this.Controls.Add(this.widthLabel);
            this.Controls.Add(this.blackAndWhiteCheckBox);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.useUICheckBox);
            this.Controls.Add(this.useAdfCheckBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.scan);
            this.Controls.Add(this.selectSource);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Scan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "合同扫描";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Scan_FormClosing);
            this.Load += new System.EventHandler(this.Frm_Scan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button selectSource;
        private System.Windows.Forms.Button scan;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox useAdfCheckBox;
        private System.Windows.Forms.CheckBox useUICheckBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.CheckBox blackAndWhiteCheckBox;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.Button diagnosticsButton;
        private System.Windows.Forms.CheckBox checkBoxArea;
        private System.Windows.Forms.CheckBox showProgressIndicatorUICheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox useDuplexCheckBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox autoRotateCheckBox;
        private System.Windows.Forms.CheckBox autoDetectBorderCheckBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

