﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Sbk_Manage
{
    public partial class Frm_Pwd : Form
    {
        public Frm_Pwd()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!Program.User_Pwd.Equals(textBox1.Text.Trim()))
            {
                MessageBox.Show("用户原密码输入有误!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (textBox2.Text.Trim().Equals("") || textBox3.Text.Trim().Equals(""))
            {
                MessageBox.Show("密码不可为空!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!textBox2.Text.Trim().Equals(textBox3.Text.Trim()))
            {
                MessageBox.Show("新密码和确认密码不一致!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Program.SqliteDB.UpdateUserPwd(Program.User_Name, textBox2.Text.Trim()) == 1)
            {
                MessageBox.Show("密码修改成功!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Program.User_Pwd = textBox2.Text.Trim();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("密码修改失败!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Frm_Pwd_Load(object sender, EventArgs e)
        {
            textBox1.KeyPress += new KeyPressEventHandler(textBox1_KeyPress);
            textBox2.KeyPress += new KeyPressEventHandler(textBox2_KeyPress);
            textBox3.KeyPress += new KeyPressEventHandler(textBox3_KeyPress);
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(System.Windows.Forms.Keys.Enter) && !textBox1.Text.Trim().Equals(""))
            {
                textBox2.Focus();
            }
        }
        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(System.Windows.Forms.Keys.Enter) && !textBox2.Text.Trim().Equals(""))
            {
                textBox3.Focus();
            }
        }
        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(System.Windows.Forms.Keys.Enter) && !textBox3.Text.Trim().Equals(""))
            {
                button1.PerformClick();
            }
        }
    }
}
